<%@ Page Language="C#" AutoEventWireup="true" CodeFile="main.aspx.cs" Inherits="main" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>数据库优化助手</title>
    <link rel="stylesheet" type="text/css" href="themes/metro/easyui.css" />
    <link rel="stylesheet" type="text/css" href="themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="style/style.css" />
    <link rel="stylesheet" type="text/css" href="style/main.css" />
    <script type="text/javascript" src="Scripts/jquery-3.1.js"></script>
    <script type="text/javascript" src="Scripts/jquery.cookie.js"></script>
    <script type="text/javascript" src="Scripts/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="Scripts/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="Scripts/main.js"></script>
</head>
<body class="easyui-layout">
    <div class="tips"></div>
    <div class="mask">
        <div class="sysParm easyui-draggable" data-options="handle:'.hdtt'">
            <div class="hdtt">
                <span class="wtt icon-cog2">系统设置</span>
                <span class="icon-close"></span>
            </div>
            <div class="setCnt">
                 <ul>
                     <li style="text-align:right;">系统名称：</li><li><input type="text" id="sysnIpt" class="hstpt" style="width:180px;" /></li>
                     <li style="text-align:right;">系统用户：</li><li><input type="text" id="uIpt" class="hstpt" style="width:180px;" /></li>
                     <li style="text-align:right;">系统版本：</li><li><input type="text" id="verIpt" class="hstpt" style="width:180px;" /></li>
                     <li style="text-align:right;">软件作者：</li><li><input type="text" id="author" class="hstpt" style="width:180px;" /></li>
                     <li style="text-align:right;">服务器IP：</li><li><input type="text" id="sIp" class="hstpt" style="width:180px;" /></li>
                     <li style="text-align:right;">权限账户：</li><li><input type="text" id="sUser" class="hstpt" style="width:180px;" /></li>
                     <li style="text-align:right;">账户密码：</li><li><input type="text" id="uPwd" class="hstpt" style="width:180px;" /></li>
                     <li style="width:100%;"><input type="button" id="subBtn" class="tjBtn easyui-linkbutton" value="更新" /></li>
                 </ul>
            </div>
        </div>
    </div>
    <div data-options="region:'north'" class="headTxt">
        <ul class="sysName">
            <li class="sysNm"><%=sysName %></li>
        </ul>
        <ul class="mToolBar">
            <li><i class="icon-gear"></i><span class="icnTxt">系统设置</span></li> 
        </ul>
    </div>
    <div data-options="region:'west',split:true,title:'数据库实例'" style="width:212px;">
        <div class="server">
            <div class="hip">主机：<input id="hstip" class="hstpt" value="127.0.01" /></div>
            <div class="newhst">更改</div>
        </div>
        <div id="setHost">
            <ul>
                <li>主机IP：</li><li style="width:120px;"><input type="text" id="sIp" class="hstpt" /></li>
                <li>用户名：</li><li style="width:120px;"><input type="text" id="dbuser" class="hstpt" /></li>
                <li>密码：</li><li style="width:120px;"><input type="password" class="hstpt" id="dbpwd" /></li>
                <li style="width:60px; float:right;margin-right:20px; margin-top:3px;"><input type="button" id="submtSet" value="提交" /></li>
            </ul>
        </div>
        <ul id="nav">
            <asp:Repeater ID="dblstRp" runat="server">
                <ItemTemplate>
                    <a class='ckcNav' href='dbpage.aspx?DbName="<%# Eval("DbName") %>"' onclick='RedirectUrl(this);return false;'><li class='NavLi'><span class='icon-database' style='color:#f90;margin-top:6px;'></span><span class='navTxt'><%# Eval("DbName") %></span></li></a>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <ul style="list-style:none;padding:0;margin:0;margin-top:10px;display:none;">
            <li class="dblst" id="hdbtn">用户数据库</li>
            <li class="dblst" id="AllBtn">所有数据库</li>
        </ul>
    </div>
    <div data-options="region:'center',border:false" style="padding:0px;overflow-y:hidden;height:100%;">
         <div id="mTabs" class="easyui-tabs" data-options="fit:true" style="padding:0;overflow-y:hidden;">           
	     <div id="defaultPage" title="任务面板" data-options="iconCls:'icon-home',fit:true"> 
              <iframe id="mIfm" src="Home.aspx" data-options="fit:true" class="db-iframe"></iframe>  
	     </div>   
	 </div>
    </div>
    <div data-options="region:'south'" style="height:26px;line-height:26px;text-indent:2em;">
        <%=sysinfo %>
    </div>
    <script type="text/javascript">
        //getArea();
		$(function(){
			$('#mTabs').layout();
			setHeight();
		});
		function setHeight(){
			var c = $('#mTabs');
			var p = c.layout('panel','center');// 获取center面板
			var oldHeight = p.panel('panel').outerHeight();
			p.panel('resize', {height:'auto'});
			var newHeight = p.panel('panel').outerHeight();
			//$("#mIfm").height = (c.height() + newHeight - oldHeight+5);
			c.layout('resize',{
				height: (c.height() + newHeight - oldHeight+5)
			});
		}
		function addItem(){
			$('#mTabs').layout('panel','center').append('<p>More Panel Content.</p>');
			setHeight();
		}
		
		function removeItem(){
			$('#mTabs').layout('panel','center').find('p:last').remove();
			setHeight();
		}
		//获取iframe中的src属性
		function RedirectUrl(t) {
		    $("#main").prop("src", t.href);
		    return false;
		};	 
    </script> 
</body>
</html>
