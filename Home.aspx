<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
          <h3>这里是赞助广告位</h3>
          <p style="text-indent:2em;">
              通过捐赠或者赞助超过一定金额，将在此位置链接您的广告。本版面根据赞助和捐赠金额，提供10个广告位。或者您一次性买断！
          </p>
          <p style="text-indent:2em;">
              根据捐赠和赞助金额差别，将有广告版面位置和大小的区别。
          </p>
          <p style="text-indent:2em;color:blue;">
              做ASP.NET开源软件，做您喜欢的开源工具！   ---IT刀客
          </p>
          <p style="text-indent:2em;color:red;">
             欢迎联系我，QQ：562318426
          </p>
    </div>
    </form>
</body>
</html>
