<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Procs.aspx.cs" Inherits="DbSql_Procs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .panelCnt{width:400px;height:100%;float:left;left:0;top:0;margin:0;padding:0;}
        .phead{width:400px;height:30px; line-height:30px;list-style:none;padding:0;background-color:#ddd;margin-top:0;}
        .plst{width:396px;padding:0; list-style:none;margin:0;height:100%;overflow-y:auto;margin-top:-5px;}
        .pli{width:208px;height:36px;list-style:none; line-height:18px;border-bottom:1px dashed #ddd;float:left;cursor:pointer;word-break:break-all;}
        .ptime{width:170px;height:30px;list-style:none; line-height:30px;border-bottom:1px dashed #ddd;float:left;border-left:1px solid #ddd;}
    </style>
    <link rel="stylesheet" type="text/css" href="/themes/metro/easyui.css" />
    <link rel="stylesheet" type="text/css" href="/themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="/style/style.css" />
    <script type="text/javascript" src="/Scripts/jquery-3.1.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.cookie.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/Scripts/easyui-lang-zh_CN.js"></script>
</head>
<body class="easyui-layout">
    <div class="panelCnt">
        <ul class="phead">
            <li style="width:176px;height:30px;list-style:none; line-height:30px;border-bottom:1px dashed #ddd;float:left;text-align:center;">存储过程名</li>
            <li style="width:176px;height:30px;list-style:none; line-height:30px;border-bottom:1px dashed #ddd;float:left;text-align:center;">创建时间</li>
        </ul>
        <ul class="plst">
            <asp:Repeater ID="prcLst" runat="server">
                <ItemTemplate>
                    <li class="pli"><%# Eval("name") %></li><li class="ptime" style="width:170px;text-align:center;"><%# Eval("crdate") %></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
    <div style="width:621px;float:left;top:0;padding:0;margin:0;margin-left:0;">
        <ul style="width:100%;float:left;margin-top:1px;list-style:none;">
            <li>
                <input class="easyui-textbox" id="procTxt" data-options="multiline:true" style="width:620px;height:360px;color:blue;" />
            </li>
        </ul>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".pli").on("click", function () {
                var pname = $(this).text();
                $(".pli").css({ "background-color": "#fff" });
                $(this).css({ "background-color": "#f90" });
                $.ajax({
                    type: 'get',
                    url: '/Assistant/getDb.ashx?DbName=' +<%=dbName %> +'&ProcName=' + pname,
                    contentType: 'application/text',
                    async: false,
                    success: function (data) {
                        if (data != '') {
                            $("#procTxt").textbox("setValue", data);
                        }
                    }
                });
            });
        });
    </script>
</body>
</html>
