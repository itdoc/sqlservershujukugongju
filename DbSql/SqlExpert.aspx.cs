using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DbSql_SqlExpert : System.Web.UI.Page
{
    protected static string dbnm = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["DbName"] != null)
        {
            dbnm = Request["DbName"].Trim();
            hdb.Value = Request["DbName"].Trim();
        }
    }
}