<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SqlExpert.aspx.cs" Inherits="DbSql_SqlExpert" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SQL优化</title>
    <link rel="stylesheet" type="text/css" href="/themes/bootstrap/easyui.css" />
    <link rel="stylesheet" type="text/css" href="/themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="/style/style.css" />
    <style type="text/css">
        .tlbar{left:0;top:0;float:left;width:99%;height:30px;}
        .tlbar ul{list-style:none;margin:0;padding:0;}
        .tlbar ul li{list-style:none;padding:5px;height:20px; line-height:20px; border-bottom:1px dashed #eee;font-size:1em;}
        .inpt{width:120px;height:20px;border-radius:5px; text-indent:5px; border:1px solid #0094ff;float:left;}
        .tlbtns{height:36px;width:100%;line-height:30px;}
        .icon-chevron-circle-right{color:red;font-size:1.3em; line-height:26px;}
        #exeRslt{border:1px solid #0094ff;border-radius:6px;height:20px;text-indent:1em;width:180px;color:red;}
    </style>
    <script type="text/javascript" src="/Scripts/jquery-3.1.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/Scripts/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="/Scripts/sqlExpert.js"></script>
</head>
<body class="easyui-layout">
    <input type="hidden" id="hdb" runat="server" />
    <div class="easyui-tabs" option-data:"fit:true"> 
		<div title="执行次数最多SQL" style="padding:5px;height:100%;">
            <table id="dgScnt" class="easyui-datagrid" data-options="singleSelect:true,collapsible:true,
                rownumbers:true,pagination:true,pageSize:10,url:'/Assistant/getData.ashx?MultiCount=10&DbName='<%=dbnm %>,method:'get'">
		        <thead>
			        <tr>
				        <th data-options="field:'sqlTxt'" style="width:460px;">执行语句</th>
                        <th data-options="field:'ExeCount'" style="width:60px;">执行次数</th>
				        <th data-options="field:'avgTime'" style="width:80px;">平均时间(ms)</th>
				        <th data-options="field:'totalTime',align:'right'" style="width:80px;">总时间(ms)</th>
				        <th data-options="field:'cpuTtime',align:'right'" style="width:100px;">CPU总时间(ms)</th>
				        <th data-options="field:'LastTime',align:'center'" style="width:160px;">最近执行时间</th>
			        </tr> 
		        </thead>
	        </table>
		</div>
		<div title="执行效率最低SQL" style="padding:5px">
			 <table id="dglow" class="easyui-datagrid" data-options="singleSelect:true,collapsible:true,
                rownumbers:true,pagination:true,pageSize:10,url:'/Assistant/getData.ashx?Low=yes&DbName='<%=dbnm %>,method:'get'">
		        <thead>
			        <tr>
				        <th data-options="field:'sqlTxt'," style="width:460px;">执行语句</th>
                        <th data-options="field:'TotalTime'" style="width:80px;">总时间(ms)</th>
				        <th data-options="field:'DBID'" style="width:80px;">数据库ID</th>
			        </tr> 
		        </thead>
	        </table>
		</div>
		<div title="测试SQL执行时间" data-options="iconCls:'icon-help'" style="padding:5px">
             <div class="tlbtns">
                 执行结果(毫秒)：<input type="text" id="exeRslt" runat="server" />
                 <a href="#" class="easyui-linkbutton" id="exeSql"><span class="icon-chevron-circle-right"></span>&nbsp;执行</a>
                 注意：此处测试的时间仅为数据库引擎执行时间，不包含.NET传输部分。
             </div>
			 <textarea id="taTxt" rows="16" style="width:100%;border:1px solid #ccc; overflow-y:auto;"></textarea>
		</div>
        <div title="存储过程执行时间" data-options="iconCls:'icon-help'" style="padding:5px">
			待开发功能
		</div>
	</div>
</body>
</html>
