using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// 获取数据库索引功能
/// 作者：IT刀客
/// </summary>
public partial class DbSql_dbIndex : System.Web.UI.Page
{
    protected string dbName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        dbName = Request["DbName"];
        DataTable dtl = dbe(dbName);
        idxLst.DataSource = dtl;
        idxLst.DataBind();
    }
    //获取数据库中表的所有索引
    protected DataTable dbe(string dname)
    {
        DataTable dte = new DataTable();
        DataTable dta = dt();
        if (dta.Rows.Count > 0)
        {
            string ip = dta.Rows[0]["hostIp"].ToString();
            string dbu = dta.Rows[0]["DbUser"].ToString();
            string dbpwd = dta.Rows[0]["DbPwd"].ToString();

            string dbStr = "Data Source=" + ip + ";DataBase=" + dbName + "; uid=" + dbu + ";pwd=" + dbpwd + "";
            string sql = @"select  a.name as tabname,h.name as idname from  sys.objects as a 
                right join sys.indexes  as h  on  a.object_id=h.object_id
                 where  a.type<>'s'";
            dte = SqlHelper.ExecuteDataSet(dbStr, sql).Tables[0];
        }
        return dte;
        
    }
    //获取数据库服务器配置参数
    protected static DataTable dt()
    {
        DataSet ds = XMLHelper.rds("App_Data/SystemSetting.xml");
        DataTable dtt = ds.Tables[0];
        return dtt;
    }
}