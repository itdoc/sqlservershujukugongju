using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DbSql_Procs : System.Web.UI.Page
{
    protected string dbName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        dbName = Request["DbName"];
        DataTable dtl = dtt(dbName);
        prcLst.DataSource = dtl;
        prcLst.DataBind();
    }
    //获取存储过程列表
    protected static DataTable dtt(string dbName)
    {
        DataTable dbe = new DataTable();
        DataTable dta = dt();
        if (dta.Rows.Count > 0)
        {
            string ip = dta.Rows[0]["hostIp"].ToString();
            string dbu = dta.Rows[0]["DbUser"].ToString();
            string dbpwd = dta.Rows[0]["DbPwd"].ToString();

            string dbStr = "Data Source=" + ip + ";DataBase=" + dbName + "; uid=" + dbu + ";pwd=" + dbpwd + "";
            string plst = "select name,Convert(nvarchar(16),crdate, 120) as crdate,refdate from dbo.sysobjects where OBJECTPROPERTY(id, N'IsProcedure') = 1 order by name";
            dbe = SqlHelper.ExecuteDataSet(dbStr, plst).Tables[0];
        }
        return dbe;
    }
    //获取数据库服务器配置参数
    protected static DataTable dt()
    {
        DataSet ds = XMLHelper.rds("App_Data/SystemSetting.xml");
        DataTable dtt = ds.Tables[0];
        return dtt;
    }
}