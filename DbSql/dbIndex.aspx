<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dbIndex.aspx.cs" Inherits="DbSql_dbIndex" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .idxul{list-style:none;padding:0;margin:0;width:600px;}
        .idxul li{list-style:none;float:left;width:260px;height:26px; line-height:26px;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ul class="idxul" style="background-color:#ccc;">
            <li>索引名称</li><li>所属表名</li>
        </ul>
        <asp:Repeater ID="idxLst" runat="server">
            <ItemTemplate>
                <ul class="idxul">
                    <li><%# Eval("idname") %></li><li style="border-left:1px solid #eee;"><%# Eval("tabname") %></li>
                </ul>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    </form>
</body>
</html>
