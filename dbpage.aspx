<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dbpage.aspx.cs" Inherits="dbpage" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" style="height:100%;overflow:hidden">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="themes/bootstrap/easyui.css" />
    <link rel="stylesheet" type="text/css" href="themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="style/style.css" />
    <style type="text/css">
        .tips{position:absolute;width:160px;height:26px; border-radius:10px;z-index:999;line-height:26px;font-size:1em;top:5%;left:50%;background-color:rgba(173,255,47,0.5);box-shadow:3px 3px 3px #808080;display:none;text-align:center;}
        #left{width:230px;top:0; border-right:1px solid #0094ff;overflow-x:hidden; overflow-y:auto;float:left;}
        #rht{float:left;width:80%; height:100%; top:0;overflow-x:hidden; overflow-y:auto;}
        .tbul{margin:0;padding:0;padding-left:1px;}
        .tbitm{height:26px; line-height:26px;list-style:none;border-bottom:1px dashed #eee; cursor:pointer; text-indent:2em;}
        .icon-table2{color:#6592d8;}
        .clms{float:left;width:80px;height:26px; line-height:26px; text-align:center;}
        .icon-steam-square{font-size:1.5em;color:#F60;}
        .icon-list {font-size:1.2em;color:green;}
        .icon-wrench {font-size:1.2em;color:green;}
    </style>
    <script type="text/javascript" src="Scripts/jquery-3.1.js"></script>
    <script type="text/javascript" src="Scripts/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="Scripts/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="Scripts/dbpage.js"></script>
</head>
<body class="easyui-layout">
    <div class="tips"></div>
    <input type="hidden" runat="server" id="htbn" />
    <div style="width:100%;height:100%;top:0;left:0;margin:0;padding:0;">
        <div id="left" style="width:220px;height:100%;">  
            <ul class="tbul easyui-panel" title="<%=rqStr %>">
            </ul>
        </div>
        <div id="rht">
            <table id="column" title="表名称" class="easyui-datagrid" data-options="singleSelect:true,
                rownumbers:true,collapsible:true,onClickCell: onClickCell,toolbar:'#tbar'" fit:"true">  
                <thead>  
                    <tr>   
                        <th data-options="field:'cloumName',width:100,editor:'text'">字段名</th>  
                        <th data-options="field:'bs',width:36,sortable:true,editor:{type:'checkbox',options:{on:'√',off:''}}">标识</th>
                        <th data-options="field:'primry',width:36,sortable:true,editor:{type:'checkbox',options:{on:'√',off:''}}">主键</th>  
                        <th data-options="field:'ctype',width:120,sortable:true,						formatter:function(value,row){
							return row.ctype;},editor:{
							type:'combobox',
							options:{
								valueField:'ctype',
								textField:'ctype',
								method:'get',
								url:'clumType.json',
								required:true
							}
						}">字段类型</th>  
                        <th data-options="field:'byteLength',width:50,sortable:true">字节数</th>
                        <th data-options="field:'cLength',width:60,sortable:true,editor:'numberbox'">字段宽度</th>  
                        <th data-options="field:'iNull',width:66,sortable:true,editor:{type:'checkbox',options:{on:'√',off:''}}">值可空</th>  
                        <th data-options="field:'deaflt',width:130,sortable:true,editor:'text'">默认值</th>  
                        <th data-options="field:'mark',width:200,sortable:true,editor:'text'">备注说明</th>    
                    </tr>  
                </thead>  
              </table>
              <div id="tbar" style="padding:5px;height:auto">
		            <div style="margin-bottom:5px">
			            <a id="BtnProc" class="easyui-linkbutton" data-options="plain:true"><span class="icon-steam-square"></span> 存储过程</a>
			            <a id="idxBtn" class="easyui-linkbutton" data-options="plain:true"><span class="icon-list"></span>索引</a>
			            <a id="btnSql" class="easyui-linkbutton" data-options="plain:true"><span class="icon-wrench"></span> SQL优化</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="append()">新增字段</a>
                        <a id="SaveBtn" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true">保存</a>
                        <a id="delet" class="easyui-linkbutton" data-options="iconCls:'icon-cancel',plain:true">删除</a>
		            </div>
              </div>
        </div>
    </div>
    <div id="cntw" class="easyui-window" title="优化助手工具" data-options="iconCls:'large_shapes',collapsible: false,modal:true,closed:true,maximized:true" style="padding:5px;">
        <iframe id="mfrm" name="mfrm" style="border:0;height:100%;width:100%;" />
    </div>
</body>
</html>
