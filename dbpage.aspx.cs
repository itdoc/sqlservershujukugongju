using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dbpage : System.Web.UI.Page
{
    protected string rqStr = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserName"]==null || Request["DbName"] ==null)
        {
            string js = "<script language=javascript>window.top.location.replace('{0}')</script>";
            HttpContext.Current.Response.Write(string.Format(js, "/login.html"));
        }
        else
        {
            rqStr = Request["DbName"].Trim();
        }
    }
}