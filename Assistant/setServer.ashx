﻿<%@ WebHandler Language="C#" Class="setServer" %>

using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Xml;

public class setServer : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        var rs = context.Response;
        var rq = context.Request;
        rs.ContentType = "text/plain";

        //设置数据库服务器参数
        if (rq["HstIp"] != null && rq["DbUser"] != null && rq["DbPwd"] != null)
        {
            string ip = rq["HstIp"].Trim();
            string user = rq["DbUser"].Trim();
            string pwd = rq["DbPwd"].Trim();
            string xmlFile = "SystemSetting.xml";
            string sysName = "数据库优化助手";
            bool rslt = XMLHelper.WriteXml(xmlFile,sysName,user,"芊鸟软件科技有限公司",ip,pwd);
            rs.Write(rslt);
            rs.End();
        }
        //获取系统设置参数
        if (rq["getSparam"] == "yes")
        {
            DataTable dt = XMLHelper.rds("App_Data/SystemSetting.xml").Tables[0];
            string jsnStr = JsonConvert.SerializeObject(dt);

            rs.Write(jsnStr);
            rs.End();
        }
        //更新系统设置
        if (rq["sNm"] != null && rq["User"] != null && rq["ver"] != null && rq["auth"] != null && rq["ip"] != null)
        {
            string sysName = rq["sNm"].Trim();
            string user = rq["User"].Trim();
            string ver = rq["ver"].Trim();
            string author = rq["auth"].Trim();
            string hip = rq["ip"].Trim();
            string dbu = rq["db"].Trim();
            string pwd = rq["uPwd"].Trim();

            string xmlFile = "SystemSetting.xml";
            bool rslt = XMLHelper.WriteXml(xmlFile,sysName,user,dbu,hip,pwd);
            rs.Write(rslt);
            rs.End();
        }
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}