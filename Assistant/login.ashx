﻿<%@ WebHandler Language="C#" Class="login" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml;

public class login : IHttpHandler,IRequiresSessionState {
    protected static readonly string ConStr = System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
    protected string SysName = "";
    protected string uName = "";
    protected string uPwd = "";
    protected int uId = 0;
    protected int RoleId = 0;
    protected int lgCount = 0;
    protected string UserImg = "";

    public void ProcessRequest (HttpContext context) {
        var r = context.Response;
        var q = context.Request;
        r.ContentType = "text/plain";
        if (q["Uname"] !=null && q["Pwd"] != null)
        {
            string user = q["Uname"].Trim();
            string pwd = MethodCls.MD5(q["Pwd"].Trim());

            context.Session["UserName"] = null;
            string loginRslt = XMLHelper.LoginResult(user, pwd);
            string[] userInfo = loginRslt.Split(',');
            uId = int.Parse(userInfo[0]);
            uName = userInfo[1];
            uPwd = userInfo[2];
            RoleId = int.Parse(userInfo[3]);
            UserImg = userInfo[4];
            lgCount = int.Parse(userInfo[5]);

            if (user == uName && pwd == uPwd)
            {
                context.Session["UserName"] = uId + "," + uName + "," + uPwd + "," + RoleId + "," + UserImg + "," + lgCount;
                
                string rslt = "true";
                r.Write(rslt);
            }
            else
            {
                context.Response.Write("0");
                context.Response.End();
            }
        }

    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}