using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Name:数据库优化助手主框架
/// By：IT刀客
/// Date:2017-04-29
/// </summary>
public partial class main : System.Web.UI.Page
{
    protected string uName = "";
    protected string smp = "";
    protected string lst = null;
    protected string sysName = "数据库优化助手";
    protected string sysinfo = "";
    protected string ip = "127.0.0.1";
    protected string dbu = "sa";
    protected string dbpwd = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserName"] !=null)
        {
            uName = Session["UserName"].ToString();
            DataTable dta = dt();
            if (dta.Rows.Count > 0)
            {
                ip = dta.Rows[0]["hostIp"].ToString();
                dbu = dta.Rows[0]["DbUser"].ToString();
                dbpwd = dta.Rows[0]["DbPwd"].ToString();
                sysName = dta.Rows[0]["systemName"].ToString();
                
                sysinfo += "著作权所有©（IT刀客）" + dta.Rows[0]["User"].ToString()+ "&nbsp;&nbsp;版本：" + dta.Rows[0]["softVertion"].ToString();
                DataTable dtb = getDbLst(ip, dbu, dbpwd);
                dblstRp.DataSource = dtb;
                dblstRp.DataBind();
            }
        }
        else
        {
            Response.Redirect("login.html");
        }
    }
    //获取数据库服务器配置参数
    protected DataTable dt()
    {
        DataSet ds = XMLHelper.rds("App_Data/SystemSetting.xml");
        DataTable dtt = ds.Tables[0];
        return dtt;
    }
    //获取数据库列表
    protected DataTable getDbLst(string ip,string usName,string dbpwd)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("DbName", typeof(string));

        System.Collections.ArrayList arrl = SqlHelper.GetAllDataBase(ip, usName, dbpwd);
        foreach (string db in arrl)
        {
            dt.Rows.Add(db);
        }
        return dt;
    }
}