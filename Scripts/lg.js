$(function () {
    document.onkeydown = function (e) {
        var ev = document.all ? window.event : e;
        if (ev.keyCode == 13) {
            $('#lgin').click();//处理事件
        }
    }
});
$(document).ready(function () {
    $("#tbxUser").focus();
    $("#lgin").on("click", function () {
        var unm = $("#tbxUser").val();
        var pwd = $("#tbxPwd").val();
        var rslt = '';
        if (unm == '' || pwd=='') {
            alert('请填写完整的登录信息！');
            return false;
        } else {
            $.ajax({
                type: 'get',
                url: 'Assistant/login.ashx?Uname=' + unm + '&Pwd=' + pwd + '',
                async: true,
                contentType: 'application/json',
                success: function (data) {
                    if (data == 'true') {
                        if ($("#stp").is(':checked')) {
                            window.location.href = "Main.aspx";
                        }
                        else {
                            $("#serverSet").fadeIn(1000);
                        }
                    }
                    else {
                        alert("用户名密码错误！");
                    }
                }
            });
        }
        
    });
    //提交数据库服务器参数
    $("#sparm").on("click", function () {
        var htip = $("#sIp").val();
        var user = $("#dbuser").val();
        var pwd = $("#dbpwd").val();
        $.ajax({
            type: 'get',
            url: 'Assistant/setServer.ashx?HstIp=' + htip + '&DbUser=' + user+'&DbPwd='+pwd,
            async: false,
            success: function (data) {
                if (data == "True") {
                    $("#serverSet").hide();
                    window.location.href = "Main.aspx";
                } 
            }
        });
    });
});