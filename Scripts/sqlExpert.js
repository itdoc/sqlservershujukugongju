$(document).ready(function () {
    //查询sql语句执行时间
    $("#exeSql").on("click", function () {
        var sqlStr = $("#taTxt").val();
        if (sqlStr == "") {
            alert('请输入SQL语句');
        } else {
            $.ajax({
                type: 'post',
                url: '/Assistant/getData.ashx',
                data: { sqlTxt: sqlStr, sqlTime: "yes", DbName:$("#hdb").val() },
                async: false,
                success: function (data) {
                    $("#exeRslt").val('');
                    $("#exeRslt").val(data*36);
                }
            });
        }
    });
});