var para = window.location.search;
var db = para.split('=');
var dbnm = db[1];//获得URL页面传递过来的参数

$(document).ready(function () {
    var tbs = "";
    $.ajax({
        type: 'get',
        url: 'Assistant/getDb.ashx?DbName=' + dbnm,
        async: false,
        contentType: 'application/json',
        success: function (data) {
            var tblst = JSON.parse(data);
            $.each(tblst.comments, function (i, item) {
                tbs += "<li class='tbitm'><span class='icon-table2'></span>&nbsp;" + item.TbName + "</li>";
            });
            $(".tbul").html(tbs);
        }
    });

    //表名的单击事件
    $(".tbitm").on("click", function () {
        $("#htbn").val('');
        $(".tbitm").css({ "background-color": "#fff" });
        $(this).css({ "background-color": "#ccc" });
        var tbname = $(this).text();
        $("#htbn").val(tbname);
        var colunms = "";
        var hander = 'Assistant/getDb.ashx?TbName=' + tbname + '&DbName=' + dbnm;
        $('#column').datagrid('options').url = hander;
        $('#column').datagrid("getPanel").panel("setTitle", tbname);
        $('#column').datagrid('reload');
    });
    //打开存储过程窗口
    $("#BtnProc").on("click", function () {
        $("#mfrm").prop("src", "DbSql/Procs.aspx?DbName=" + dbnm);
        $(".panel-with-icon").text("存储过程");
        $("#cntw").window('open');
    });
    //打开索引窗口
    $("#idxBtn").on("click", function () {
        $("#mfrm").prop("src", "DbSql/dbIndex.aspx?DbName=" + dbnm);
        $(".panel-with-icon").text("数据库索引");
        $("#cntw").window('open');
    });
    //打开SQL诊断优化窗口
    $("#btnSql").on("click", function () {
        $("#mfrm").prop("src", "DbSql/SqlExpert.aspx?DbName=" + dbnm);
        $(".panel-with-icon").text("SQL诊断&优化");
        $("#cntw").window('open');
    });
    //保存表字段更改
    $("#SaveBtn").on("click", function () {
        var tbnm = $("#htbn").val();
        var row = $('#column').datagrid('getSelected');
        var json = [];
        var cloms;
        var clmName = "";
        if (row) {
            clmName = row.cloumName;
            cloms = {
                "cloumName": row.cloumName,
                "bs": row.bs,
                "primry": row.primry,
                "ctype": row.ctype,
                "byteLength": row.byteLength,
                "cLength": row.cLength,
                "iNull": row.iNull,
                "defalt": row.defalt,
                "mark": row.mark
            }
            json.push(cloms);
            json = JSON.stringify(json); //转换成json数据 
            $.ajax({
                type: 'get',
                url: '/Assistant/WriteData.ashx?rowDt=' + json + '&DbName=' + dbnm + '&TbName=' + tbnm + '&clumName=' + clmName,
                //data: { rowDt: json, DbName: dbnm, TbName: tbnm, clumName: clmName },
                contentType: 'application/json',
                success: function (data) {
                    if (data == 0) {
                        $(".tips").html('');
                        $(".tips").html('保存失败');
                        $(".tips").show();
                        setTimeout(function () {
                            $(".tips").hide();
                        }, 1000);
                        
                    } else {
                        $(".tips").html('');
                        $(".tips").html('<span class="icon-check" style="color:green"></span>保存成功');
                        $(".tips").show();
                        setTimeout(function () {
                            $(".tips").hide();
                        }, 1000);
                    }
                }
            });
        }
    });

    //删除字段
    $("#delet").on("click", function () {
        var tbnm = $("#htbn").val();
        var row = $('#column').datagrid('getSelected');
        var clmName = "";
        var prim = "";
        if (row) {
            clmName = row.cloumName;
            prim = row.primry;
            $.ajax({
                type: 'get',
                url: '/Assistant/WriteData.ashx?deletClum=yes&DbName=' + dbnm + '&TbName=' + tbnm + '&clumName=' + clmName+'&primry='+prim,
                contentType: 'application/json',
                success: function (data) {
                    if (data == 0) {
                        $(".tips").html('');
                        $(".tips").html('保存失败');
                        $(".tips").show();
                        setTimeout(function () {
                            $(".tips").hide();
                        }, 1000);

                    } else {
                        $(".tips").html('');
                        $(".tips").html('<span class="icon-check" style="color:green"></span>保存成功');
                        $(".tips").show();
                        setTimeout(function () {
                            $(".tips").hide();
                        }, 1000);
                    }
                }
            })
        }
    });


});
$.extend($.fn.datagrid.methods, {
    editCell: function (jq, param) {
        return jq.each(function () {
            var opts = $(this).datagrid('options');
            var fields = $(this).datagrid('getColumnFields', true).concat($(this).datagrid('getColumnFields'));
            for (var i = 0; i < fields.length; i++) {
                var col = $(this).datagrid('getColumnOption', fields[i]);
                col.editor1 = col.editor;
                if (fields[i] != param.field) {
                    col.editor = null;
                }
            }
            $(this).datagrid('beginEdit', param.index);
            for (var i = 0; i < fields.length; i++) {
                var col = $(this).datagrid('getColumnOption', fields[i]);
                col.editor = col.editor1;
            }
        });
    }
});

var editIndex = undefined;
function endEditing() {
    if (editIndex == undefined) { return true }
    if ($('#column').datagrid('validateRow', editIndex)) {
        $('#column').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
//新增字段
function append() {
    if (endEditing()) {
        $('#column').datagrid('appendRow', { status: 'P' });
        editIndex = $('#column').datagrid('getRows').length - 1;
        $('#column').datagrid('selectRow', editIndex)
                .datagrid('beginEdit', editIndex);
    }
}
function onClickCell(index, field) {
    if (endEditing()) {
        $('#column').datagrid('selectRow', index)
                .datagrid('editCell', { index: index, field: field });
        editIndex = index;

        //var rowsData = $('#column').datagrid('getRows');
        //var json = [];
        //var loc;
        //$.each(rowsData, function (i) {
        //    loc = {
        //        "cloumName": rowsData[i].cloumName,
        //        "bs": rowsData[i].bs,
        //        "primry": rowsData[i].primry,
        //        "ctype": rowsData[i].ctype,
        //        "byteLength": rowsData[i].byteLength,
        //        "cLength": rowsData[i].cLength
        //    };
        //    json.push(loc);
        //});
        //json = JSON.stringify(json); //转换成json数据 
    }
}