$(document).ready(function () {
    //$("#hstip").val('Local');
    //重新设置数据库实例参数
    $(".newhst").on("click", function () { 
        $("#setHost").slideToggle(500);
    });
    //设置数据库服务器参数文本框获得焦点事件
    $(".hstpt").focus(function () {
        $(this).css({ "border": "1px solid #9aadc8" });
    });
    //设置数据库服务器参数文本框失去焦点事件
    $(".hstpt").blur(function () {
        if ($(this).val() == '') {
            $(this).css({ "border": "1px solid red" });
        }
    });
    //鼠标经过主框架工具栏事件
    $(".mToolBar li").on("mouseover", function () {
        $(this).css({ "background": "#adc2f3","color":"#fff" });
    });
    //鼠标离开主框架工具栏事件
    $(".mToolBar li").on("mouseleave", function () {
        $(this).css({ "background": "transparent","color":"#000" });
    });
    //系统设置单击事件
    $(".mToolBar li").on("click", function () {
        $.ajax({
            type: 'get',
            url: 'Assistant/setServer.ashx?getSparam=yes',
            contentType: 'application/json',
            async: false,
            success: function (data) {
                var sjsn = JSON.parse(data);
                $.each(sjsn, function (i, itm) {
                    $("#sysnIpt").val(itm.systemName);
                    $("#uIpt").val(itm.User);
                    $("#verIpt").val(itm.softVertion);
                    $("#author").val(itm.techSupport);
                    $("#sIp").val(itm.hostIp);
                    $("#sUser").val(itm.DbUser);
                    $("#uPwd").val(itm.DbPwd);
                })
            }
        });
        $(".sysParm").show();
        $(".mask").show();
    });
    //提交系统设置事件
    $("#subBtn").on("click", function () {
        var snm = $("#sysnIpt").val();
        var sUser = $("#uIpt").val();
        var sftVer = $("#verIpt").val();
        var author = $("#author").val();
        var hstIp = $("#sIp").val();
        var dbu =$("#sUser").val();
        var pwd = $("#uPwd").val();
        $.ajax({
            type: 'post',
            url: 'Assistant/setServer.ashx',
            data: {sNm:snm,User:sUser,ver:sftVer,auth:author,ip:hstIp,db:dbu,uPwd:pwd},
            success: function (data) {
                if(data=="True"){
                    $(".tips").html('');
                    $(".tips").html('<span class="icon-check" style="color:green"></span>&nbsp;保存成功');
                    $(".tips").show();
                    setTimeout(function () {
                        $(".tips").hide();
                    }, 1000);
                }
            }
        })
    });
    //鼠标经过关闭按钮
    $(".icon-close").on("mouseover", function () {
        $(this).css({"color":"#000","background-color":"#bbb"});
    });
    //鼠标离开关闭按钮
    $(".icon-close").on("mouseleave", function () {
        $(this).css({ "color": "#aaa","background-color":"#ddd"});
    });
    //关闭按钮事件
    $(".icon-close").on("click", function () {
        $(".mask").hide();
        $(".sysParm").hide();
    });
    //提交对数据库服务器的更改
    $("#submtSet").on("click", function () {
        var ip = $("#sIp").val();
        var unm = $("#dbuser").val();
        var dbpwd = $("#dbpwd").val();
        if (ip != "" && unm != '' && dbpwd != '') {
            $(".hstpt").css({ "border": "1px solid #9aadc8" });
            var nmlst = "";
            $.ajax({
                type: 'get',
                url: 'Assistant/getDb.ashx?uName=' + unm + '&uPwd=' + dbpwd + '&ip=' + ip + '&geDbLst=All',
                success: function (data) {
                    var obj = JSON.parse(data);
                    $.each(obj.comments, function (i, item) {
                        nmlst += "<a class='ckcNav' href='dbpage.aspx?DbName=" + item.DbName + "' onclick='RedirectUrl(this);return false;'><li class='NavLi'><span class='icon-database' style='color:#f90;margin-top:6px;'></span><span class='navTxt'>" + item.DbName + "</span></li></a>";
                    });
                    $("#nav").html('');
                    $("#nav").html(nmlst);
                }
            });
            $("#hstip").val(ip);
            $("#setHost").slideToggle(500);
        } 
    });
    //获取非系统数据库列表（用户数据库）
    $("#hdbtn").on("click", function () {
        var nmlst = "";
        $.ajax({
            type: 'get',
            url: 'Assistant/getDb.ashx?uName=sa&uPwd=1105&ip=127.0.0.1&geDbLst=only',
            success: function (data) {
                var obj = JSON.parse(data);
                $.each(obj.comments, function (i, item) {
                    nmlst += "<a class='ckcNav' href='dbpage.aspx?DbName=" + item.DbName + "' onclick='RedirectUrl(this);return false;'><li class='NavLi'><span class='icon-database' style='color:#f90;margin-top:6px;'></span><span class='navTxt'>" + item.DbName + "</span></li></a>";
                })
                $("#nav").html('');
                $("#nav").html(nmlst);
            }
        });
    });
    //获取某个实例下所有数据库列表
    $("#AllBtn").on("click", function () {
        var nmlst = "";
        $.ajax({
            type: 'get',
            url: 'Assistant/getDb.ashx?uName=sa&uPwd=1105&ip=127.0.0.1&geDbLst=All',
            success: function (data) {
                var obj = JSON.parse(data);
                $.each(obj.comments, function (i, itm) {
                    nmlst += "<a href='home.aspx' onclick='RedirectUrl(this);return false;'><li class='NavLi'><span class='icon-database' style='color:#f90;'></span><span class='navTxt'>" + itm.DbName + "</span></li></a>";
                })
                $("#nav").html("");
                $("#nav").html(nmlst);
            }
        });
    });
    //数据库点击样式
    $(".ckcNav").on("click", function () {
        $(".ckcNav").css({ "background": "blue" });
        $(this).css({ "background": "#f90" });
    });
    //获取当前数据库中包含的所有表
    //$("#nav li").on("click", function () {
    //    var index = $("#nav li").index(this);
    //    $(".tblst").eq(index).show();
    //});
    //导航菜单点击切换URL事件
    $(".ckcNav").on("click", function () {
        var tabTitle = $(this).text();
        var $this = $(this);
        var icon = "icon-cog";
        icon = $(this).children("li").children("span").attr('class');
        var icn = icon.split(' ');
        icon = icn[0] + " icnHgt";
        addTab(tabTitle, this.href, icon);
    });
    function addTab(title, url,icon) {
        if ($('#mTabs').tabs('exists', title)) {
            $('#mTabs').tabs('select', title);
        } else {
            var content = '<iframe class="db-iframe" src="' + url + '"></iframe>';
            $('#mTabs').tabs('add', {
                title: title,
                iconCls: icon,
                content: content,
                closable: true
            });
        }
    }
    //获取iframe中的src属性
    function RedirectUrl(t) {
        $("#main").prop("src", t.href);
        return false;
    };
});